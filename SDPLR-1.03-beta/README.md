
SDPLR is a C software package for solving large-scale semidefinite programming problems
=====================================================================================


- Source code is download from <http://dollar.biz.uiowa.edu/~sburer/projects.html>

- Userguide can be found at <http://dollar.biz.uiowa.edu/~sburer/files/SDPLR-1.03-beta-usrguide.pdf>


- A precompiled OSX version is included (OSX 10.9, Homebrew GCC 4.8).


Original authors are:
- Samuel Burer (samuel-burer@uiowa.edu)
- Renato D.C. Monteiro (monteiro@isye.gatech.edu)
- Changhui Choi (changhui.choi@ucdenver.edu)


